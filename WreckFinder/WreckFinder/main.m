//
//  main.m
//  WreckFinder
//
//  Created on 29/5/17.
//  Copyright © 2017 WreckFinder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
